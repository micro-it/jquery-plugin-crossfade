# README #

Simple jQuery plugin for crossfading alements on your page

### Options ###
**fadeTime**

Default: 300 miliseconds

**delay**

Default: 10000 miliseconds

### Example ###
Simple example:
~~~~
jQuery(".crossfade").CrossFade();
~~~~

Example with options:
~~~~
jQuery(".crossfade").CrossFade({
    fadeTime: 500,
    delay: 10000
});
~~~~