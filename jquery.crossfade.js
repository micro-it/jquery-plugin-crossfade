/*
 * jQuery CrossFade 1.0
 * 
 * Created by: Jeroen el Hahaoui <j.el.hahaoui@micro-it.nl>
 */
jQuery.fn.CrossFade = function(options)
{
    var settings = jQuery.extend({
        fadeTime: 300,
        delay: 10000
    }, options);
    
        
    jQuery(this.selector).each(function() {
        $this = jQuery(this);
        var childs = $this.children();

        var slide_active = 0;
        var slide_length = childs.length;

        // Hide all slides, except the first slide
        jQuery.each(childs, function(index) {
            if (index != slide_active)
            {
                jQuery(this).hide();
            }
        });

        setInterval(function() {
            slide_active++;

            // Reset counter
            if (slide_active == slide_length)
            {
                slide_active = 0;
            }

            // Hide each page
            var hide_element, show_element;
            jQuery.each(childs, function(index, currentElement) {
                if (index != slide_active)
                {
                    jQuery(this).fadeOut(settings.fadeTime);
                    
                }
                else
                {
                    var elm = jQuery(this);
                    setTimeout(function() {elm.fadeIn(settings.fadeTime)}, settings.fadeTime);
                }
            });
            
        }, settings.delay);
    
    });
    return this;
}